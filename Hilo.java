// Lograr que los hilos muestren su mensaje de identificacion en el orden en el que son creados
public class Hilo implements Runnable{
	private int id;
	private static Object lock = new Object();
	private static int cont = 0;
	
	public Hilo(int id)
	{
		this.id = id;
	}
	
	@Override
	public void run() 
	{
		synchronized(lock)							// El while dentro de esto es la condicion de guarda.
		{
			while(this.id != cont)					// Primero debera ejecutarse el hilo con id 0, este hilo no entrara al while y por lo  
			{										// tanto no se dormira. Si un hilo con id !=0 entra al synchronized, entrara al while
				try {								// y se dormira las veces que sea necesario hasta que cont se haya incrementado hasta el
					lock.wait();					// valor de su id (que es lo mismo que decir que los hilos anteriores se hayan ejecutado)
				} catch (InterruptedException e) {}
			}
			System.out.println("Hola, soy el hilo "+this.id);
			cont++;
			lock.notifyAll();						// notifyAll despierta todos los hilos, tanto los que estan en la cola del synchronized
		}											// como los que fueron dormidos por un wait.
	}
}
